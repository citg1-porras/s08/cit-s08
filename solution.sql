-- a.Find all artists with d in its name
SELECT * FROM artists WHERE name LIKE "%d%";

-- b.Find all songs that has a length of less than 230
SELECT * FROM songs WHERE length < 230;

-- c.Join the albums and songs tables;
SELECT album_title, song_name,length FROM albums JOIN songs ON songs.album_id=albums.id;

-- d.Join artists and albums tables
SELECT * FROM artists JOIN albums ON artists.id=albums.artist_id WHERE albums.album_title LIKE "%a%";

-- e. Sorting the albums in Z-A
SELECT * FROM albums ORDER BY album_title DESC LIMIT 4;

-- f. Join the albums and songs tables
SELECT * from albums JOIN songs ON songs.album_id=albums.id order by albums.album_title DESC,songs.song_name ASC;